# Installation

- First, install **ruby 2.0** or greater and **rubygems**
- Then, install bundler ``gem install bundler``.
- Then install the dependencies ``bundle install``


# Usage

    Usage: ./Requireris [ -s=SECRET | -u=ACCOUNT | --qrcode=file ] [ -t | -c=COUNT ]
            --debug
        -v, --verbose
        -t, --time                       Usage of TOTP, based on the timestamp (RFC 6238)
        -c, --count=C                    Usage of HOTP, based on a counter (RFC 4226)
        -s, --secret=S                   Set the secret (required)
            --time-split=TS              Set the time split (default: 30, in seconds)
        -d, --digits=NB                  Set the number of digits (default: 6)
        -e, --encode                     Encode the secret in Base32
        -q, --qrcode=FILE                Read QrCode from image file (require: zbar, zbarimg)
        -l, --account-list               List every accounts
        -u, --account-use=ACCOUNT        Use the secret of an account
            --google-auth, --google=CODE https://accounts.google.com/b/0/SmsAuthSettings#devices
        -r, --register=ACCOUNT           Register this secret for an account


# HTTP API

    cd ApiOTP
    bundle install
    nserver -p 8080

Then you can access to the api via 2 routes:

    curl http://localhost:8080/hotp/NRXWY/1
    curl http://localhost:8080/totp/NRXWY

