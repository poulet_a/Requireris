#encoding: utf-8

require 'base32'
require 'digest'
require 'openssl'

module OTP

  def self.hmac(digest, secret, input)
    OpenSSL::HMAC.digest(
      OpenSSL::Digest.new(digest),
      secret,
      input)
  end

  def self.computes(secret, message_origin, digits=6)
    key = decode_secret(secret)
    message = stringify_int(message_origin).rjust(8, "\x0")
    hash = hmac("sha1", key, message) # HMAC with SHA-1
    #puts "secret: #{key}"
    #puts "input: #{message_origin}"
    #puts "inut2: #{message.bytes}"
    #puts "Hmac: #{hash}"

    offset = hash[-1].ord & 0x0f
    #puts "Offset: #{offset}"

    # 4 bytes starting at the offset
    truncatedHash = hash[offset..offset+3]
    th_first_char = truncatedHash[0].ord & 127
    truncatedHash[0] = th_first_char.chr

    truncatedHashInt = 0
    truncatedHash.bytes.each{|b| truncatedHashInt = (truncatedHashInt << 8) | (b & 0xff)}
    code = truncatedHashInt % (10**digits)
    #puts "Code: #{truncatedHash}"
    #puts "Code: #{code}"

    return code.to_s.rjust(6, "0")
  end

  def self.totp(opt) #secret, digits
    message = (Time.now.utc.to_i / 30)
    computes(opt[:secret], message, opt[:digits])
  end
  def self.valid_for()
    ((Time.now.utc.to_i / 30).round + 1) * 30
  end

  def self.hotp(opt) #secret, counter, digits
    computes(opt[:secret], opt[:counter] || opt[:count], opt[:digits])
  end

  RFC = {
    totp: {method: :totp, arguments: 2},
    hotp: {method: :htop, arguments: 3}
  }
  def self.computes_type(type, opt)
    call = RFC[type]
    self.send(call[:method], opt)
  end

  def self.encode_secret(s)
    Base32.encode(s.to_s)
  end

  def self.decode_secret(s)
    Base32.decode(s.to_s)
  end

  def self.stringify_int(int)
    Math.log10(int).ceil.times.map do |i|
      ((int >> (8 * i)) & 0xff).chr
    end.join.reverse
  end

end
