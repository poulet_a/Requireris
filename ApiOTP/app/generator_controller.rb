require_relative '../OTP'

DIGITS = 6

class GeneratorController < Nephos::Controller
  def totp
    secret = params[:secret]
    secret = Base32.encode(secret) if params[:encode] == "true"
    code = OTP.totp({secret: secret, digits: DIGITS})
    return {json: {code: code, validity: Time.at(OTP.valid_for)}}
  end

  def hotp
    secret = params[:secret]
    secret = Base32.encode(secret) if params[:encode] == "true"
    code = OTP.hotp({secret: secret, count: Integer(params[:count]), digits: DIGITS})
    return {json: {code: code, count: Integer(params[:count])}}
  end
end
