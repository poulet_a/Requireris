#encoding: utf-8

require 'optparse'
require 'base32'
require 'json'

ACCOUNTS_FILE = ".accounts.json"

module Requireris
  class Argument

    attr_reader :mode, :secret, :count, :time_split, :digits
    def initialize
      @mode = :totp
      @count = 0
      @time_split = 30
      @digits = 6
      @encode = false
      @verbose = false
      @register = nil
      if not File.exists? ACCOUNTS_FILE
        File.write(ACCOUNTS_FILE, [].to_json)
        File.chmod(0600, ACCOUNTS_FILE)
      end
      @accounts = JSON.parse(File.read(ACCOUNTS_FILE))

      @opts = OptionParser.new do |opts|
        opts.banner = "Usage: ./Requireris [ -s=SECRET | -u=ACCOUNT | --qrcode=file ] [ -t | -c=COUNT ]"

        # Options HOTP / TOTP
        opts.on("--time", "-t", "Usage of TOTP, based on the timestamp (RFC 6238)") do
          @mode = :topt
        end

        opts.on("--count=C", "-c=C", "Usage of HOTP, based on a counter (RFC 4226)") do |c|
          @mode = :hotp
          @count = Integer(c)
        end

        # Ouput format
        opts.on("--time-split=TS", "Set the time split (default: 30, in seconds)") do |ts|
          @time_split = Integer(ts)
        end
        opts.on("--digits=NB", "-d=NB", "Set the number of digits (default: 6)") do |nb|
          @digits = Integer(nb)
        end

        # Input format
        opts.on("--secret=S", "-s=S", "Set the secret (required)") do |s|
          @secret = s
        end
        opts.on("--qrcode=FILE", "-q=FILE", "Read QrCode from image file (require: zbar, zbarimg)") do |filename|
          exists = `whereis zbarimg` != "zbarimg:\n"
          raise "Please install zbar (zbarimg binary required)" unless exists
          @qrcode = filename
        end
        opts.on("--google-auth=CODE", "--google", "https://accounts.google.com/b/0/SmsAuthSettings#devices") do |secret|
          @secret = secret.split.join
          Base32.decode @secret
          @encode = false
        end
        opts.on("--encode", "-e", "Encode the secret in Base32") do
          @encode = true
        end


        # Account handlement
        opts.on("--account-list", "-l", "List every accounts") do
          len = @accounts.map{|e|e["account"].size}.max
          @accounts.each_with_index do |data, idx|
            puts "#{idx} : #{data["account"].ljust(len)} : #{data["secret"]}"
          end
          exit 0
        end
        opts.on("--account-use=ACCOUNT", "-u=ACCOUNT", "Use the secret of an account") do |account|
          begin
            @secret = Base32.decode(@accounts.find{|e| e["account"] == account}["secret"])
          rescue => _err
            puts "No account registered for \"#{account}\""
            exit 1
          end
        end
        opts.on("--register=ACCOUNT", "-r=ACCOUNT", "Register this secret for an account") do |account|
          @register = account
        end

        # Others
        opts.on("--debug") do
          $debug = true
          STDERR.puts "Debug on"
        end
        opts.on("--verbose", "-v") do
          @verbose = true
        end

      end.parse!

      ### Secret handlement ###
      # qrcode
      if @qrcode
        @secret = `zbarimg -q '#{@qrcode}'`
        raise "Invalid QRCode file" unless @secret.match(/QR-Code:.+\n/m)
        @secret = @secret[8..-2]
        m = @secret.match(/otpauth:\/\/.+secret=([a-zA-Z0-9]+).+/)
        if m
          @secret = m[1]
          begin
            Base32.decode(@secret)
          rescue
            @secret = Base32.encode(@secret)
          end
        end
      # from text
      else
        # --secret not required if plain option (./Requireris KEY equal to ./Requireris -s KEY)
        @secret ||= @opts.first

        # no secret, read on stdin
        unless @secret
          print "> "
          @secret = STDIN.gets.to_s.chomp
        end

        # Base32 if required
        if @encode
          @secret = Base32.encode(@secret)
          puts "Secret encoded: #{@secret}" if @verbose
        end
      end

      # register account
      if @register
        @accounts << {"account" => @register, "secret" => @secret}
        @accounts.uniq!
        File.write(ACCOUNTS_FILE, @accounts.to_json)
      end
    end

  end
end
